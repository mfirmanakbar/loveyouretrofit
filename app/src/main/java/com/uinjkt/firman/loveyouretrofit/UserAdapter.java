package com.uinjkt.firman.loveyouretrofit;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * Created by firmanmac on 7/30/17.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private List<User.ItemsBean> itemsBean;

    public UserAdapter(List<User.ItemsBean> itemsBean) {
        this.itemsBean = itemsBean;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.each_row_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(itemsBean.get(position).getLogin());
        holder.score.setText(String.valueOf(itemsBean.get(position).getScore()));
        Uri imageUri = Uri.parse(itemsBean.get(position).getAvatar_url());
        holder.img_ava.setImageURI(imageUri);
    }

    @Override
    public int getItemCount() {
        return itemsBean.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView title, score;
        public SimpleDraweeView img_ava;
        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.txtTitle);
            score = (TextView)itemView.findViewById(R.id.txtCount);
            img_ava = (SimpleDraweeView)itemView.findViewById(R.id.img_ava);
        }
    }

}
