package com.uinjkt.firman.loveyouretrofit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    //https://api.github.com/search/users?q=tom

    public static final String BASE_URL = "https://api.github.com/";
    private static final String TAG = "MainActivity";

    private RecyclerView rv_user;
    private UserAdapter userAdapter;
    private List<User.ItemsBean> Users;
    private RecyclerView.LayoutManager layoutManager;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fresco.initialize(this);

        rv_user = (RecyclerView)findViewById(R.id.rv_user);

        //tanpa kolom
        /*layoutManager = new LinearLayoutManager(getApplicationContext());
        rv_user.setLayoutManager(layoutManager);*/

        //pake kolom
        layoutManager = new GridLayoutManager(this, 2);
        rv_user.setLayoutManager(layoutManager);
        rv_user.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.feedswipe);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServer();
            }
        });

        //loadServer();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadServer();
    }

    private void loadServer() {
        Log.d("compareRet2:", "Mulai");
        mSwipeRefreshLayout.setRefreshing(true);
        OkHttpClient ohc = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Accept", "Application/JSON").build();
                return chain.proceed(request);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(ohc)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyApiEndpointEnterface service = retrofit.create(MyApiEndpointEnterface.class);

        Call<User> call = service.getUserNamedTom("tom");
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                if (response.isSuccessful()){
                    Users = new ArrayList<User.ItemsBean>();
                    User result = response.body();
                    Users = result.getItems();
                    userAdapter = new UserAdapter(Users);
                    rv_user.setAdapter(userAdapter);
                    Log.d("compareRet2:", "Selesai");
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getApplicationContext(),"Connection failed!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClickToMenuRxJava(View view){
        Intent a = new Intent(MainActivity.this, RxjavaActivity.class);
        startActivity(a);
    }

    public void onClickToMenuLoopj(View view){
        Intent a = new Intent(MainActivity.this, LoopjActivity.class);
        startActivity(a);
    }

}
