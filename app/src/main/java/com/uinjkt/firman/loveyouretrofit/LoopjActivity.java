package com.uinjkt.firman.loveyouretrofit;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class LoopjActivity extends AppCompatActivity {

    //https://api.github.com/search/users?q=tom

    public static final String BASE_URL = "https://api.github.com/";
    private static final String TAG = "loopjx";

    private RecyclerView rv_user;
    private UserAdapter userAdapter;
    private List<User.ItemsBean> Users;
    private RecyclerView.LayoutManager layoutManager;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loopj);

        Fresco.initialize(this);

        rv_user = (RecyclerView)findViewById(R.id.rv_user);

        //tanpa kolom
        /*layoutManager = new LinearLayoutManager(getApplicationContext());
        rv_user.setLayoutManager(layoutManager);*/

        //pake kolom
        layoutManager = new GridLayoutManager(this, 2);
        rv_user.setLayoutManager(layoutManager);
        rv_user.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.feedswipe);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServer();
            }
        });

        //loadServer();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadServer();
    }

    private void loadServer() {
        mSwipeRefreshLayout.setRefreshing(true);
        Log.d("compareLoopJ","Mulai");
        String url = BASE_URL + "search/users";
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Accept", "Application/JSON");
        client.addHeader("Content-Type", "Application/JSON");
        client.addHeader("User-Agent", "http.agent");
        RequestParams params = new RequestParams();
        params.put("q", "tom");
        client.get(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (statusCode==200){
                    Users = new ArrayList<User.ItemsBean>();
                    User result = new Gson().fromJson(response.toString(), User.class);
                    Users = result.getItems();
                    userAdapter = new UserAdapter(Users);
                    rv_user.setAdapter(userAdapter);
                    Log.d("compareLoopJ","Selesai");
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                Log.d(TAG,"xonFailure "+String.valueOf(statusCode));
                Log.d(TAG,"xonFailure "+String.valueOf(headers));
                Log.d(TAG,"xonFailure "+res);
                Log.d(TAG,"xonFailure "+String.valueOf(t));
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }
}
