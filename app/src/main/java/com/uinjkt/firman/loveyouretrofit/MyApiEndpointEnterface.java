package com.uinjkt.firman.loveyouretrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by firmanmac on 7/30/17.
 */

public interface MyApiEndpointEnterface {

    @GET("search/users")
    Call<User> getUserNamedTom(@Query("q") String name);

}

